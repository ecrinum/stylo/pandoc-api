FROM python:3.11-slim-buster

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update -y && apt-get upgrade -y \
    && apt-get install -y make wget nano vim
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --fix-missing \
            texlive-latex-base texlive-latex-extra texlive-luatex
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --fix-missing \
            texlive-lang-english texlive-lang-french texlive-lang-spanish \
            texlive-lang-portuguese texlive-lang-german texlive-lang-italian \
            texlive-lang-cyrillic texlive-lang-greek fonts-oldstandard
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y default-jdk
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get clean -y && rm -rf /var/lib/apt/lists/*

ARG TARGETARCH

ENV PANDOC_VERSION="3.6.3"
ENV PANDOC_DEB="pandoc-$PANDOC_VERSION-1-$TARGETARCH.deb"
RUN wget "https://github.com/jgm/pandoc/releases/download/$PANDOC_VERSION/$PANDOC_DEB"
RUN dpkg -i $PANDOC_DEB

RUN addgroup --uid 1000 pandocapi
RUN adduser --disabled-login --ingroup pandocapi --uid 1000 pandocapi
USER pandocapi:pandocapi

WORKDIR /usr/pandoc-api

ENV VIRTUAL_ENV=/usr/pandoc-api/venv
RUN python -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN mkdir ./static ./tmp
RUN pip install gunicorn
COPY --chown=pandocapi:pandocapi Makefile setup.* .
COPY --chown=pandocapi:pandocapi app ./app
COPY --chown=pandocapi:pandocapi saxon ./saxon
COPY --chown=pandocapi:pandocapi README.md ./README.md
COPY --chown=pandocapi:pandocapi pyproject.toml ./pyproject.toml
RUN make install
COPY --chown=pandocapi:pandocapi .envdocker ./.env
ENV TEXINPUTS=.:/usr/pandoc-api/tmp/:

EXPOSE 8000
CMD ["make", "production"]
