from pathlib import Path

HERE = Path().resolve()
STATIC_FOLDER = HERE / "static"
TMP_FOLDER = HERE / "tmp"
API_VERSION = 20241218
