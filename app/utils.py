import os
from pathlib import Path
from typing import Any, Tuple
from zipfile import BadZipFile, ZipFile, is_zipfile

import pypandoc
from dotenv import load_dotenv

from .config import *  # noqa

load_dotenv()

try:
    print(f"Pandoc version: {pypandoc.get_pandoc_version()}")
except OSError:
    pypandoc.download_pandoc(version="3.6.3")
    print(f"Pandoc version: {pypandoc.get_pandoc_version()}")


def get_pandoc_version() -> Tuple[int, int]:
    PANDOC_VERSION = pypandoc.get_pandoc_version()
    MAJOR, MINOR = map(int, PANDOC_VERSION.split(".", 2)[:2])
    return MAJOR, MINOR


PANDOC_MAJOR, PANDOC_MINOR = get_pandoc_version()


def extract_zip(zip_file: Path, target: Path) -> None:
    if not is_zipfile(zip_file):
        return
    try:
        with ZipFile(zip_file) as zipfile:
            # Pretty basic check of integrity.
            test_result = zipfile.testzip()
            if test_result is not None:
                raise BadZipFile(f"Zipfile contains a bad file: {zip_file}")
            for filename in zipfile.namelist():
                zipfile.extract(filename, target)
    except BadZipFile:
        return


def get_env_var(name: str, default: Any = None) -> Any:
    """Get the env var from a .env file, fallback on config.py."""
    env_var = os.getenv(name)
    if env_var is None:
        try:
            # This is quite ugly.
            env_var = globals()[name]
        except KeyError as e:
            print(f"Env var `{name}` not found.")
            if default is not None:
                return default
            else:
                raise e
    return env_var
