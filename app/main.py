from typing import Union

from fastapi import FastAPI, File, HTTPException, Query, UploadFile
from fastapi.responses import FileResponse, HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi_versioning import VersionedFastAPI, version

from .converters import (
    convert_to_docx,
    convert_to_html,
    convert_to_icml,
    convert_to_odt,
    convert_to_pdf,
    convert_to_tex,
    convert_to_xml_erudit,
    convert_to_xml_tei,
)
from .utils import extract_zip, get_env_var

PANDOC_MAJOR = get_env_var("PANDOC_MAJOR")
PANDOC_MINOR = get_env_var("PANDOC_MINOR")
PANDOC_VERSION = int(f"{PANDOC_MAJOR}{PANDOC_MINOR}")
API_VERSION = get_env_var("API_VERSION")
TMP_FOLDER = get_env_var("TMP_FOLDER")
STATIC_FOLDER = get_env_var("STATIC_FOLDER")

app = FastAPI()


@app.post(
    "/convert/html/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"text/html": {}},
            "description": "Return the generated HTML file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_html(
    name: str = Query(
        default="index.html",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].html$",
        description="Name of the generated HTML file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    with_toc: bool = Query(
        default=False,
        description="Add a Table Of Contents to the document.",
        required=True,
    ),
    with_ascii: bool = Query(
        default=False,
        description="Encode HTML entities.",
        required=True,
    ),
    with_link_citations: bool = Query(
        default=False,
        description="Link citations to bibliography’s entries.",
        required=True,
    ),
    with_nocite: bool = Query(
        default=False,
        description="Keep even unused citations within bibliography.",
        required=True,
    ),
    standalone: bool = Query(
        default=True,
        description="Put the document within an HTML/CSS template.",
        required=False,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    csl_file: UploadFile = File(description="The CSL file."),
    template_file: Union[UploadFile, None] = File(
        description="The template file.", default=None
    ),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    if template_file is not None:
        (TMP_FOLDER / template_file.filename).write_bytes(template_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())

    export_file_path = STATIC_FOLDER / name
    try:
        convert_to_html(
            export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            csl_file,
            template_file,
            **{
                "with_toc": with_toc,
                "with_ascii": with_ascii,
                "with_link_citations": with_link_citations,
                "with_nocite": with_nocite,
                "standalone": standalone,
            },
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])
    return FileResponse(export_file_path, media_type="text/html", filename=name)


@app.post(
    "/convert/xml/tei/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"text/xml": {}},
            "description": "Return the generated XML file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_xml_tei(
    name: str = Query(
        default="index.tei.xml",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].tei.xml$",
        description="Name of the generated TEI file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    with_link_citations: bool = Query(
        default=False,
        description="Link citations to bibliography’s entries.",
        required=True,
    ),
    with_nocite: bool = Query(
        default=False,
        description="Keep even unused citations within bibliography.",
        required=True,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    template_file: UploadFile = File(description="The template file."),
    csl_file: UploadFile = File(description="The CSL file."),
    xsl_file: Union[UploadFile, None] = File(
        default=None, description="The XSL file (optional)."
    ),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    (TMP_FOLDER / template_file.filename).write_bytes(template_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())
    if xsl_file is not None:
        (TMP_FOLDER / xsl_file.filename).write_bytes(xsl_file.file.read())

    export_file_path = STATIC_FOLDER / name
    tmp_export_file_path = STATIC_FOLDER / f"tmp-{name}"
    try:
        convert_to_xml_tei(
            export_file_path,
            tmp_export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            template_file,
            csl_file,
            xsl_file,
            **{
                "with_link_citations": with_link_citations,
                "with_nocite": with_nocite,
            },
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])

    return FileResponse(export_file_path, media_type="text/xml", filename=name)


@app.post(
    "/convert/xml/erudit/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"text/xml": {}},
            "description": "Return the generated XML file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_xml_erudit(
    name: str = Query(
        default="index.erudit.xml",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].erudit.xml$",
        description="Name of the generated Erudit file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    template_file: UploadFile = File(description="The template file."),
    csl_file: UploadFile = File(description="The CSL file."),
    xsl_file: Union[UploadFile, None] = File(
        default=None, description="The XSL file (optional)."
    ),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    (TMP_FOLDER / template_file.filename).write_bytes(template_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())
    if xsl_file is not None:
        (TMP_FOLDER / xsl_file.filename).write_bytes(xsl_file.file.read())

    export_file_path = STATIC_FOLDER / name
    tmp_export_file_path = STATIC_FOLDER / f"tmp-{name}"
    try:
        convert_to_xml_erudit(
            export_file_path,
            tmp_export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            template_file,
            csl_file,
            xsl_file,
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])

    return FileResponse(export_file_path, media_type="text/xml", filename=name)


@app.post(
    "/convert/tex/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"text/tex": {}},
            "description": "Return the generated TEX file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_tex(
    name: str = Query(
        default="index.tex",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].tex$",
        description="Name of the generated TEX file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    with_toc: bool = Query(
        default=False,
        description="Add a Table Of Contents to the document.",
        required=True,
    ),
    with_link_citations: bool = Query(
        default=False,
        description="Link citations to bibliography’s entries.",
        required=True,
    ),
    with_nocite: bool = Query(
        default=False,
        description="Keep even unused citations within bibliography.",
        required=True,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    template_file: UploadFile = File(description="The template file."),
    csl_file: UploadFile = File(description="The CSL file."),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    (TMP_FOLDER / template_file.filename).write_bytes(template_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())

    export_file_path = STATIC_FOLDER / name
    try:
        convert_to_tex(
            export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            template_file,
            csl_file,
            **{
                "with_toc": with_toc,
                "with_link_citations": with_link_citations,
                "with_nocite": with_nocite,
            },
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])

    return FileResponse(export_file_path, media_type="text/tex", filename=name)


@app.post(
    "/convert/pdf/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"application/pdf": {}},
            "description": "Return the generated PDF file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_pdf(
    name: str = Query(
        default="output.pdf",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].pdf$",
        description="Name of the generated PDF file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    with_toc: bool = Query(
        default=False,
        description="Add a Table Of Contents to the document.",
        required=True,
    ),
    with_link_citations: bool = Query(
        default=False,
        description="Link citations to bibliography’s entries.",
        required=True,
    ),
    with_nocite: bool = Query(
        default=False,
        description="Keep even unused citations within bibliography.",
        required=True,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    images_file: UploadFile = File(description="The images archive."),
    template_file: UploadFile = File(description="The template file."),
    csl_file: UploadFile = File(description="The CSL file."),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    (TMP_FOLDER / template_file.filename).write_bytes(template_file.file.read())
    (TMP_FOLDER / images_file.filename).write_bytes(images_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())

    (TMP_FOLDER / template_file.filename).write_text(
        (TMP_FOLDER / template_file.filename)
        .read_text()
        .replace("{{graphicspath}}", f"{{{{{TMP_FOLDER}}}}}")
    )
    extract_zip(TMP_FOLDER / images_file.filename, TMP_FOLDER)

    export_file_path = STATIC_FOLDER / name
    try:
        convert_to_pdf(
            export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            template_file,
            csl_file,
            **{
                "with_toc": with_toc,
                "with_link_citations": with_link_citations,
                "with_nocite": with_nocite,
            },
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])

    return FileResponse(export_file_path, media_type="application/pdf", filename=name)


@app.post(
    "/convert/odt/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"text/odt": {}},
            "description": "Return the generated odt file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_odt(
    name: str = Query(
        default="index.odt",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].odt$",
        description="Name of the generated odt file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    with_toc: bool = Query(
        default=False,
        description="Add a Table Of Contents to the document.",
        required=True,
    ),
    with_link_citations: bool = Query(
        default=False,
        description="Link citations to bibliography’s entries.",
        required=True,
    ),
    with_nocite: bool = Query(
        default=False,
        description="Keep even unused citations within bibliography.",
        required=True,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    images_file: UploadFile = File(description="The images archive."),
    csl_file: UploadFile = File(description="The CSL file."),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())
    (TMP_FOLDER / images_file.filename).write_bytes(images_file.file.read())

    extract_zip(TMP_FOLDER / images_file.filename, TMP_FOLDER)

    export_file_path = STATIC_FOLDER / name
    try:
        convert_to_odt(
            export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            csl_file,
            **{
                "with_toc": with_toc,
                "with_link_citations": with_link_citations,
                "with_nocite": with_nocite,
            },
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])

    return FileResponse(export_file_path, media_type="text/odt", filename=name)


@app.post(
    "/convert/docx/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"text/docx": {}},
            "description": "Return the generated docx file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_docx(
    name: str = Query(
        default="index.docx",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].docx$",
        description="Name of the generated docx file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    with_toc: bool = Query(
        default=False,
        description="Add a Table Of Contents to the document.",
        required=True,
    ),
    with_link_citations: bool = Query(
        default=False,
        description="Link citations to bibliography’s entries.",
        required=True,
    ),
    with_nocite: bool = Query(
        default=False,
        description="Keep even unused citations within bibliography.",
        required=True,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    images_file: UploadFile = File(description="The images archive."),
    csl_file: UploadFile = File(description="The CSL file."),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())
    (TMP_FOLDER / images_file.filename).write_bytes(images_file.file.read())

    extract_zip(TMP_FOLDER / images_file.filename, TMP_FOLDER)

    export_file_path = STATIC_FOLDER / name
    try:
        convert_to_docx(
            export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            csl_file,
            **{
                "with_toc": with_toc,
                "with_link_citations": with_link_citations,
                "with_nocite": with_nocite,
            },
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])

    return FileResponse(export_file_path, media_type="text/docx", filename=name)


@app.post(
    "/convert/icml/",
    response_class=FileResponse,
    responses={
        200: {
            "content": {"text/icml": {}},
            "description": "Return the generated icml file.",
        }
    },
)
@version(API_VERSION, PANDOC_VERSION)
async def convert_icml(
    name: str = Query(
        default="index.icml",
        pattern="^[a-zA-Z0-9_.-]*[a-zA-Z0-9_.-].icml$",
        description="Name of the generated icml file.",
        min_length=8,
        max_length=150,
        required=True,
    ),
    markdown_file: UploadFile = File(description="The markdown file."),
    bibtex_file: UploadFile = File(description="The bibtex file."),
    yaml_file: UploadFile = File(description="The YAML file."),
    csl_file: UploadFile = File(description="The CSL file."),
) -> FileResponse:
    (TMP_FOLDER / yaml_file.filename).write_bytes(yaml_file.file.read())
    (TMP_FOLDER / bibtex_file.filename).write_bytes(bibtex_file.file.read())
    (TMP_FOLDER / csl_file.filename).write_bytes(csl_file.file.read())

    export_file_path = STATIC_FOLDER / name
    try:
        convert_to_icml(
            export_file_path,
            TMP_FOLDER,
            markdown_file,
            bibtex_file,
            yaml_file,
            csl_file,
        )
    except RuntimeError as exc:
        raise HTTPException(status_code=500, detail=exc.args[0])

    return FileResponse(export_file_path, media_type="text/icml", filename=name)


# Must be done after the versionned views.
app = VersionedFastAPI(app, enable_latest=True)


# Must be done after `VersionedFastAPI()` call because we want to be
# able to access the root without any versionning.
@app.get("/")
async def root() -> HTMLResponse:
    html_content = f"""
    <html>
        <head>
            <title>An API for Pandoc</title>
        </head>
        <body>
            <h1>
                <a href="/v{API_VERSION}_{PANDOC_VERSION}/docs">
                    Try out the API!
                </a>
            </h1>
            <ul>
                <li>API version: {API_VERSION}</li>
                <li>Pandoc version: {PANDOC_MAJOR}.{PANDOC_MINOR}</li>
            </ul>
        </body>
    </html>
    """
    return HTMLResponse(content=html_content, status_code=200)


# Must be done after `VersionedFastAPI()` call, see
# https://github.com/DeanWay/fastapi-versioning/issues/36
app.mount("/static", StaticFiles(directory=STATIC_FOLDER), name="static")
