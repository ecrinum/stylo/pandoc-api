import logging
import shlex
import shutil
import subprocess
from pathlib import Path
from typing import Optional, Union

import pypandoc
from fastapi import UploadFile

from .utils import get_env_var

logging.getLogger("pypandoc").addHandler(logging.FileHandler("pypandoc.log"))


def add_metadata_to(yaml_file: Path, metadata: str) -> None:
    """Some metadata, like nocite, are not passed so we add it manually.

    Even with `pandoc_extra_args.append("--metadata=nocite:'@*'")`,
    it might depends on our current version of Pandoc (3.1.6.2).
    """
    with yaml_file.open(mode="r+") as f:
        content = f.read()
        if "nocite" not in content:
            content = content.strip(" \n-")
            f.seek(0)
            f.write(f"---\n{content}\n{metadata}\n---\n")


def convert_to_html(
    export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    csl_file: UploadFile,
    template_file: Optional[UploadFile],
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--citeproc",
        f"--metadata-file={metadata_file}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
        "--section-divs",
        "--verbose",
        f"--log={export_file_path}.log",
    ]
    if template_file is not None:
        pandoc_extra_args.append(
            f"--template={tmp_folder / (template_file.filename or 'default.template')}"
        )
    if kwargs.get("with_toc"):
        pandoc_extra_args.append("--toc")
    if kwargs.get("with_ascii"):
        pandoc_extra_args.append("--ascii")
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    if kwargs.get("standalone", True):
        pandoc_extra_args.append("--standalone")

    converted_text = pypandoc.convert_text(
        markdown_file.file.read(),
        "html",
        format="md",
        extra_args=pandoc_extra_args,
    )
    export_file_path.write_text(converted_text)


def convert_to_xml_tei(
    export_file_path: Path,
    tmp_export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    template_file: UploadFile,
    csl_file: UploadFile,
    xsl_file: Union[UploadFile, None],
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--standalone",
        "--citeproc",
        f"--metadata-file={tmp_folder / (yaml_file.filename or 'default.yaml')}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--template={tmp_folder / (template_file.filename or 'default.template')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
        "--section-divs",
    ]
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    converted_text = pypandoc.convert_text(
        markdown_file.file.read(),
        "tei",
        format="md",
        extra_args=pandoc_extra_args,
    )
    if xsl_file is None:
        export_file_path.write_text(converted_text)
    else:
        tmp_export_file_path.write_text(converted_text)
        command_parts = shlex.split(get_env_var("XSL_COMMAND"))
        absolute_executable = shutil.which(command_parts[0])
        if not absolute_executable:
            raise Exception(f"Missing executable: {command_parts[0]}")
        extra_executable = []
        if len(command_parts) > 1:
            extra_executable = command_parts[1:]
        file_parameters = [
            f"-o:{export_file_path}",
            f"-s:{tmp_export_file_path}",
            str((tmp_folder / (xsl_file.filename or "default.xsl"))),
            "!indent=yes",
        ]
        parameters = [absolute_executable, *extra_executable, *file_parameters]
        print("Running command:", subprocess.list2cmdline(parameters))
        try:
            subprocess.check_output(parameters, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as exc:
            raise RuntimeError(exc.output.decode())


def convert_to_xml_erudit(
    export_file_path: Path,
    tmp_export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    template_file: UploadFile,
    csl_file: UploadFile,
    xsl_file: Union[UploadFile, None],
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--standalone",
        "--citeproc",
        f"--metadata-file={tmp_folder / (yaml_file.filename or 'default.yaml')}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--template={tmp_folder / (template_file.filename or 'default.template')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
        "--section-divs",
    ]
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    converted_text = pypandoc.convert_text(
        markdown_file.file.read(),
        "html",
        format="md",
        extra_args=pandoc_extra_args,
    )
    if xsl_file is None:
        export_file_path.write_text(converted_text)
    else:
        tmp_export_file_path.write_text(converted_text)
        command_parts = shlex.split(get_env_var("XSL_COMMAND"))
        absolute_executable = shutil.which(command_parts[0])
        if not absolute_executable:
            raise Exception(f"Missing executable: {command_parts[0]}")
        extra_executable = []
        if len(command_parts) > 1:
            extra_executable = command_parts[1:]
        file_parameters = [
            f"-o:{export_file_path}",
            f"-s:{tmp_export_file_path}",
            str((tmp_folder / (xsl_file.filename or "default.xsl"))),
            "!indent=yes",
        ]
        parameters = [absolute_executable, *extra_executable, *file_parameters]
        print("Running command:", subprocess.list2cmdline(parameters))
        try:
            subprocess.check_output(parameters, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as exc:
            raise RuntimeError(exc.output.decode())


def convert_to_tex(
    export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    template_file: UploadFile,
    csl_file: UploadFile,
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--standalone",
        "--citeproc",
        f"--metadata-file={tmp_folder / (yaml_file.filename or 'default.yaml')}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--template={tmp_folder / (template_file.filename or 'default.template')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
    ]
    if kwargs.get("with_toc"):
        pandoc_extra_args.append("--toc")
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    pypandoc.convert_text(
        markdown_file.file.read(),
        "latex",
        format="md",
        extra_args=pandoc_extra_args,
        outputfile=str(export_file_path),
    )


def convert_to_pdf(
    export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    template_file: UploadFile,
    csl_file: UploadFile,
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--standalone",
        "--citeproc",
        f"--metadata-file={tmp_folder / (yaml_file.filename or 'default.yaml')}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--template={tmp_folder / (template_file.filename or 'default.template')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
        f"--resource-path=.:{tmp_folder}",
        "--pdf-engine=lualatex",
    ]
    if kwargs.get("with_toc"):
        pandoc_extra_args.append("--toc")
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    pypandoc.convert_text(
        markdown_file.file.read(),
        "pdf",
        format="markdown+rebase_relative_paths",
        extra_args=pandoc_extra_args,
        outputfile=str(export_file_path),
    )


def convert_to_odt(
    export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    csl_file: UploadFile,
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--standalone",
        "--citeproc",
        f"--metadata-file={tmp_folder / (yaml_file.filename or 'default.yaml')}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
        f"--resource-path=.:{tmp_folder}",
    ]
    if kwargs.get("with_toc"):
        pandoc_extra_args.append("--toc")
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    pypandoc.convert_text(
        markdown_file.file.read(),
        "odt",
        format="markdown+rebase_relative_paths",
        extra_args=pandoc_extra_args,
        outputfile=str(export_file_path),
        sandbox=False,
    )


def convert_to_docx(
    export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    csl_file: UploadFile,
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--standalone",
        "--citeproc",
        f"--metadata-file={tmp_folder / (yaml_file.filename or 'default.yaml')}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
        f"--resource-path=.:{tmp_folder}",
    ]
    if kwargs.get("with_toc"):
        pandoc_extra_args.append("--toc")
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    pypandoc.convert_text(
        markdown_file.file.read(),
        "docx",
        format="markdown+rebase_relative_paths",
        extra_args=pandoc_extra_args,
        outputfile=str(export_file_path),
        sandbox=False,
    )


def convert_to_icml(
    export_file_path: Path,
    tmp_folder: Path,
    markdown_file: UploadFile,
    bibtex_file: UploadFile,
    yaml_file: UploadFile,
    csl_file: UploadFile,
    **kwargs: bool,
) -> None:
    metadata_file = tmp_folder / (yaml_file.filename or "default.yaml")
    if kwargs.get("with_nocite"):
        add_metadata_to(metadata_file, metadata="nocite: '@*'")
    pandoc_extra_args = [
        "--standalone",
        "--citeproc",
        f"--metadata-file={tmp_folder / (yaml_file.filename or 'default.yaml')}",
        f"--bibliography={tmp_folder / (bibtex_file.filename or 'default.bib')}",
        f"--csl={tmp_folder / (csl_file.filename or 'default.csl')}",
        f"--resource-path=.:{tmp_folder}",
    ]
    if kwargs.get("with_link_citations"):
        pandoc_extra_args.append("--metadata=link-citations:true")
    pypandoc.convert_text(
        markdown_file.file.read(),
        "icml",
        format="markdown+rebase_relative_paths",
        extra_args=pandoc_extra_args,
        outputfile=str(export_file_path),
    )
