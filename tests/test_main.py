import json
import shlex
import shutil
from pathlib import Path
from unittest.mock import patch

import pytest
from fastapi.testclient import TestClient

from app.main import app
from app.utils import get_env_var

XSL_COMMAND = get_env_var("XSL_COMMAND", default="dummy")

# At some point, it would be great to install an XSL transformer on the CI.
command_parts = shlex.split(XSL_COMMAND)
hasXSLCommandExecutable = pytest.mark.skipif(
    shutil.which(command_parts[0]) is None,
    reason=f"The `{command_parts[0]}` executable is not available.",
)

PANDOC_MAJOR = get_env_var("PANDOC_MAJOR")
PANDOC_MINOR = get_env_var("PANDOC_MINOR")
API_VERSION = get_env_var("API_VERSION")
CURRENT_VERSION = f"v{API_VERSION}_{PANDOC_MAJOR}{PANDOC_MINOR}"

client = TestClient(app)


def test_root():
    response = client.get("/")
    assert response.status_code == 200
    assert "Try out the API!" in response.text


def test_convert_html():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "index.html"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="index.html"'
    )
    assert response.content.decode().startswith(
        """<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:dc="http://purl.org/dc/terms/"""
    )
    assert '<nav id="TOC">' not in response.content.decode()
    assert (
        "Stylo est un\néditeur d’article scientifique dédié aux sciences humaines."
        in response.content.decode()
    )
    assert '<a href="#ref-goody_raison_1979"' not in response.content.decode()
    assert "adams_light_2000" not in response.content.decode()


def test_convert_html_too_short_name():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "a.html"}, files=files)
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "type": "string_too_short",
                "loc": ["query", "name"],
                "msg": "String should have at least 8 characters",
                "input": "a.html",
                "ctx": {"min_length": 8},
            }
        ]
    }


def test_convert_html_not_standalone():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url, params={"name": "index.html", "standalone": "false"}, files=files
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="index.html"'
    )
    assert response.content.decode().startswith(
        """<section id="introduction" class="level2">
<h2>Introduction</h2>
<p>Stylo est un éditeur de texte scientifique. Pour faire vos premiers
pas sur Stylo, commencez par éditer cet article.</p>
"""
    )


def test_convert_html_latest_version():
    url = "/latest/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "index.html"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="index.html"'
    )


def test_convert_html_with_custom_name():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "custom.html"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="custom.html"'
    )


def test_convert_html_with_toc():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url, params={"name": "index-with-toc.html", "with_toc": "true"}, files=files
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="index-with-toc.html"'
    )
    assert response.content.decode().startswith(
        """<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:dc="http://purl.org/dc/terms/"""
    )
    assert '<nav id="TOC">' in response.content.decode()


def test_convert_html_with_ascii():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url, params={"name": "index.html", "with_ascii": "true"}, files=files
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="index.html"'
    )
    assert response.content.decode().startswith(
        """<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:dc="http://purl.org/dc/terms/"""
    )
    assert (
        "Stylo est un\n&#xE9;diteur d&#x2019;article scientifique d&#xE9;di&#xE9; aux"
        in response.content.decode()
    )


def test_convert_html_with_link_citations():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url, params={"name": "index.html", "with_link_citations": "true"}, files=files
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="index.html"'
    )
    assert response.content.decode().startswith(
        """<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:dc="http://purl.org/dc/terms/"""
    )
    assert '<nav id="TOC">' not in response.content.decode()
    assert (
        "Stylo est un\néditeur d’article scientifique dédié aux sciences humaines."
        in response.content.decode()
    )
    assert '<a href="#ref-goody_raison_1979"' in response.content.decode()
    assert "adams_light_2000" not in response.content.decode()


def test_convert_html_with_nocite():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url, params={"name": "index.html", "with_nocite": "true"}, files=files
    )
    assert response.status_code == 200, response.content.decode()
    assert response.headers["content-type"] == "text/html; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="index.html"'
    )
    assert response.content.decode().startswith(
        """<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:dc="http://purl.org/dc/terms/"""
    )
    assert '<nav id="TOC">' not in response.content.decode()
    assert (
        "Stylo est un\néditeur d’article scientifique dédié aux sciences humaines."
        in response.content.decode()
    )
    assert '<a href="#ref-goody_raison_1979"' not in response.content.decode()
    assert "adams_light_2000" in response.content.decode()


def test_convert_html_with_pandoc_error():
    url = f"/{CURRENT_VERSION}/convert/html/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateHtml5.html5").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "index.html"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


def test_convert_xml_tei():
    url = f"/{CURRENT_VERSION}/convert/xml/tei/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-tei.tei").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "index.tei.xml"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/xml; charset=utf-8"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="index.tei.xml"'
    )
    assert response.content.decode().startswith(
        """<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">"""
    )


def test_convert_xml_tei_with_error():
    url = f"/{CURRENT_VERSION}/convert/xml/tei/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-tei.tei").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "index.tei.xml"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


def test_convert_xml_tei_with_custom_name():
    url = f"/{CURRENT_VERSION}/convert/xml/tei/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-tei.tei").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "custom.tei.xml"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/xml; charset=utf-8"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="custom.tei.xml"'
    )


def test_convert_xml_tei_with_link_citations():
    url = f"/{CURRENT_VERSION}/convert/xml/tei/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-tei.tei").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_link_citations": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/xml; charset=utf-8"
    assert (
        '<ref target="#ref-goody_raison_1979">Goody 1979</ref>'
        in response.content.decode()
    )


def test_convert_xml_tei_with_nocite():
    url = f"/{CURRENT_VERSION}/convert/xml/tei/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-tei.tei").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_nocite": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/xml; charset=utf-8"
    assert "<nocite>@*</nocite>" in response.content.decode()


@hasXSLCommandExecutable
def test_convert_xml_tei_xsl_metopes():
    url = f"/{CURRENT_VERSION}/convert/xml/tei/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-tei.tei").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
        "xsl_file": Path("tests/fixtures/template-metopes.xsl").open("rb"),
    }
    response = client.post(url, params={"name": "index.tei.xml"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/xml; charset=utf-8"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="index.tei.xml"'
    )
    assert response.content.decode().startswith(
        """<?xml version="1.0" encoding="UTF-8"?>
<!--Ceci est le document output.-->
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">"""
    )


@hasXSLCommandExecutable
def test_convert_xml_tei_xsl_erudit():
    url = f"/{CURRENT_VERSION}/convert/xml/tei/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-tei.tei").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
        "xsl_file": Path("tests/fixtures/template-erudit.xsl").open("rb"),
    }
    response = client.post(url, params={"name": "index.tei.xml"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/xml; charset=utf-8"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="index.tei.xml"'
    )
    assert response.content.decode().startswith(
        """<?xml version="1.0" encoding="UTF-8"?>
<article xmlns="http://www.erudit.org/xsd/article"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.erudit.org/xsd/article \
http://www.erudit.org/xsd/article/3.0.0/eruditarticle.xsd"
         qualtraitement="complet"
         idproprio="sptemp"
         typeart="article"
         lang=""
         ordseq=" ">
   <admin>"""
    )


@hasXSLCommandExecutable
def test_convert_xml_erudit():
    url = f"/{CURRENT_VERSION}/convert/xml/erudit/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-xhtml.html").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
        "xsl_file": Path("tests/fixtures/template-erudit.xsl").open("rb"),
    }
    response = client.post(url, params={"name": "index.erudit.xml"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/xml; charset=utf-8"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="index.erudit.xml"'
    )
    assert response.content.decode().startswith(
        """<?xml version="1.0" encoding="UTF-8"?>
<article xmlns="http://www.erudit.org/xsd/article"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.erudit.org/xsd/article \
http://www.erudit.org/xsd/article/3.0.0/eruditarticle.xsd"
         qualtraitement="complet"
         idproprio="sptemp"
         typeart="article"
         lang="fr"
         ordseq=" ">
   <admin>"""
    )


def test_convert_xml_erudit_with_error():
    url = f"/{CURRENT_VERSION}/convert/xml/erudit/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-xhtml.html").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "index.erudit.xml"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


@hasXSLCommandExecutable
def test_convert_xml_erudit_with_iframe_error():
    url = f"/{CURRENT_VERSION}/convert/xml/erudit/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test-with-iframe-error.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/template-xhtml.html").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
        "xsl_file": Path("tests/fixtures/template-erudit.xsl").open("rb"),
    }
    response = client.post(url, params={"name": "index.erudit.xml"}, files=files)
    assert response.status_code == 500
    assert (
        json.loads(response.content.decode())["detail"]
        == """\
Error on line 287 column 252 of tmp-index.erudit.xml:
  SXXP0003   Error reported by XML parser: \
Le nom d'attribut "allowfullscreen" associé à un
  type d'élément "iframe" doit être suivi du caractère '='.
SXXP0003   Error reported by XML parser: Le nom d'attribut "allowfullscreen" \
associé à un type d'élément "iframe" doit être suivi du caractère '='.
"""
    )


def test_convert_pdf():
    url = f"/{CURRENT_VERSION}/convert/pdf/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "output.pdf"}, files=files)
    assert response.status_code == 200, response.content
    assert response.headers["content-type"] == "application/pdf"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="output.pdf"'
    )
    # Locally: 69931
    print(f"69000 < {int(response.headers['content-length'])} < 70000", end=" ")
    assert 69000 < int(response.headers["content-length"]) < 70000


def test_convert_pdf_sens_public():
    url = f"/{CURRENT_VERSION}/convert/pdf/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX-sens-public.latex").open(
            "rb"
        ),
        "images_file": Path("tests/fixtures/media-sens-public.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "output.pdf"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/pdf"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="output.pdf"'
    )
    # Locally: 73374
    print(f"73000 < {int(response.headers['content-length'])} < 74000", end=" ")
    assert 73000 < int(response.headers["content-length"]) < 74000


def test_convert_pdf_with_error():
    url = f"/{CURRENT_VERSION}/convert/pdf/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "output.pdf"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


def test_convert_pdf_with_custom_name():
    url = f"/{CURRENT_VERSION}/convert/pdf/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "custom.pdf"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/pdf"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="custom.pdf"'
    )
    # Locally: 69931
    print(f"69000 < {int(response.headers['content-length'])} < 70000", end=" ")
    assert 69000 < int(response.headers["content-length"]) < 70000


def test_convert_pdf_with_toc():
    url = f"/{CURRENT_VERSION}/convert/pdf/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url, params={"name": "output-with-toc.pdf", "with_toc": "true"}, files=files
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/pdf"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="output-with-toc.pdf"'
    )
    # Locally: 72282
    print(f"72000 < {int(response.headers['content-length'])} < 73000", end=" ")
    assert 72000 < int(response.headers["content-length"]) < 73000


def test_convert_pdf_with_nocite():
    url = f"/{CURRENT_VERSION}/convert/pdf/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url,
        params={"name": "output-with-nocite.pdf", "with_nocite": "true"},
        files=files,
    )
    assert response.status_code == 200, response.content.decode()
    assert response.headers["content-type"] == "application/pdf"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="output-with-nocite.pdf"'
    )
    # Locally: 71865
    print(f"71000 < {int(response.headers['content-length'])} < 72000", end=" ")
    assert 71000 < int(response.headers["content-length"]) < 72000


def test_convert_pdf_with_link_citations():
    url = f"/{CURRENT_VERSION}/convert/pdf/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url,
        params={
            "name": "output-with-link_citations.pdf",
            "with_link_citations": "true",
        },
        files=files,
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/pdf"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="output-with-link_citations.pdf"'
    )
    # Locally: 70100
    print(f"70000 < {int(response.headers['content-length'])} < 71000", end=" ")
    assert 70000 < int(response.headers["content-length"]) < 71000


def test_convert_odt():
    url = f"/{CURRENT_VERSION}/convert/odt/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "output.odt"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/odt; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="output.odt"'
    )
    # Locally 19375
    print(f"19000 < {int(response.headers['content-length'])} < 19500", end=" ")
    assert 19000 < int(response.headers["content-length"]) < 19500


def test_convert_odt_with_error():
    url = f"/{CURRENT_VERSION}/convert/odt/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "output.odt"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


def test_convert_odt_with_custom_name():
    url = f"/{CURRENT_VERSION}/convert/odt/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "custom.odt"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/odt; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="custom.odt"'
    )
    print(f"19000 < {int(response.headers['content-length'])} < 21000", end=" ")
    assert 19000 < int(response.headers["content-length"]) < 21000


def test_convert_odt_with_toc():
    url = f"/{CURRENT_VERSION}/convert/odt/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_toc": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/odt; charset=utf-8"
    # Locally 19650
    print(f"19500 < {int(response.headers['content-length'])} < 20000", end=" ")
    assert 19500 < int(response.headers["content-length"]) < 20000


def test_convert_odt_with_nocite():
    url = f"/{CURRENT_VERSION}/convert/odt/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_nocite": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/odt; charset=utf-8"
    # Locally 19717
    print(f"19000 < {int(response.headers['content-length'])} < 20000", end=" ")
    assert 19000 < int(response.headers["content-length"]) < 20000


def test_convert_odt_with_link_citations():
    url = f"/{CURRENT_VERSION}/convert/odt/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_link_citations": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/odt; charset=utf-8"
    # Locally 19629
    print(f"19000 < {int(response.headers['content-length'])} < 20000", end=" ")
    assert 19000 < int(response.headers["content-length"]) < 20000


def test_convert_docx():
    url = f"/{CURRENT_VERSION}/convert/docx/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "output.docx"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/docx; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="output.docx"'
    )
    # Locally: 23340
    print(f"23000 < {int(response.headers['content-length'])} < 23500", end=" ")
    assert 23000 < int(response.headers["content-length"]) < 23500


def test_convert_docx_with_error():
    url = f"/{CURRENT_VERSION}/convert/docx/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "output.docx"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


def test_convert_docx_with_custom_name():
    url = f"/{CURRENT_VERSION}/convert/docx/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "custom.docx"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/docx; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="custom.docx"'
    )


def test_convert_docx_with_toc():
    url = f"/{CURRENT_VERSION}/convert/docx/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_toc": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/docx; charset=utf-8"
    # Locally: 23495
    print(f"23000 < {int(response.headers['content-length'])} < 24000", end=" ")
    assert 23000 < int(response.headers["content-length"]) < 24000


def test_convert_docx_with_nocite():
    url = f"/{CURRENT_VERSION}/convert/docx/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_nocite": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/docx; charset=utf-8"
    # Locally 23472
    print(f"23000 < {int(response.headers['content-length'])} < 24000", end=" ")
    assert 23000 < int(response.headers["content-length"]) < 24000


def test_convert_docx_with_link_citations():
    url = f"/{CURRENT_VERSION}/convert/docx/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "images_file": Path("tests/fixtures/images.zip").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_link_citations": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/docx; charset=utf-8"
    # Locally 23387
    print(f"23000 < {int(response.headers['content-length'])} < 24000", end=" ")
    assert 23000 < int(response.headers["content-length"]) < 24000


def test_convert_icml():
    url = f"/{CURRENT_VERSION}/convert/icml/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "output.icml"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/icml; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="output.icml"'
    )
    assert response.content.decode().startswith(
        """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?aid style="50" type="snippet" readerVersion="6.0" featureSet="513" product="8.0(370)"\
 ?>
<?aid SnippetType="InCopyInterchange"?>
<Document DOMVersion="8.0" Self="pandoc_doc">
    <RootCharacterStyleGroup Self="pandoc_character_styles">
      <CharacterStyle Self="$ID/NormalCharacterStyle" Name="Default" />
      <CharacterStyle Self="CharacterStyle/" Name="">
        <Properties>
          <BasedOn type="object">$ID/NormalCharacterStyle</BasedOn>"""
    )


def test_convert_icml_with_error():
    url = f"/{CURRENT_VERSION}/convert/icml/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "output.icml"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


def test_convert_icml_with_custom_name():
    url = f"/{CURRENT_VERSION}/convert/icml/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "custom.icml"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/icml; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="custom.icml"'
    )
    assert int(response.headers["content-length"]) == 46331


def test_convert_tex():
    url = f"/{CURRENT_VERSION}/convert/tex/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "index.tex"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/tex; charset=utf-8"
    assert response.headers["content-disposition"] == 'attachment; filename="index.tex"'
    assert (
        "C'est article est un exemple d'article type édité sur \\emph{Stylo}."
        in response.content.decode()
    )
    assert "\\tableofcontents" not in response.content.decode()


def test_convert_tex_with_error():
    url = f"/{CURRENT_VERSION}/convert/tex/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    with patch("pypandoc.convert_text", side_effect=RuntimeError("mocked error")):
        response = client.post(url, params={"name": "index.tex"}, files=files)
    assert response.status_code == 500
    assert response.headers["content-type"] == "application/json"
    assert response.content.decode() == '{"detail":"mocked error"}'


def test_convert_tex_with_custom_name():
    url = f"/{CURRENT_VERSION}/convert/tex/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"name": "custom.tex"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/tex; charset=utf-8"
    assert (
        response.headers["content-disposition"] == 'attachment; filename="custom.tex"'
    )


def test_convert_tex_with_toc():
    url = f"/{CURRENT_VERSION}/convert/tex/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(
        url, params={"name": "output-with-toc.tex", "with_toc": "true"}, files=files
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/tex; charset=utf-8"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="output-with-toc.tex"'
    )
    assert (
        "C'est article est un exemple d'article type édité sur \\emph{Stylo}."
        in response.content.decode()
    )
    assert "\\tableofcontents" in response.content.decode()


def test_convert_tex_with_nocite():
    url = f"/{CURRENT_VERSION}/convert/tex/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_nocite": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/tex; charset=utf-8"
    assert (
        "C'est article est un exemple d'article type édité sur \\emph{Stylo}."
        in response.content.decode()
    )
    assert (
        "\\bibitem[\\citeproctext]{ref-adams_light_2000}\nAdams, Timothy Dow. 2000."
        in response.content.decode()
    )


def test_convert_tex_with_link_citations():
    url = f"/{CURRENT_VERSION}/convert/tex/"
    files = {
        "markdown_file": Path("tests/fixtures/un-test.md").open("rb"),
        "bibtex_file": Path("tests/fixtures/un-test.bib").open("rb"),
        "yaml_file": Path("tests/fixtures/un-test.yaml").open("rb"),
        "template_file": Path("tests/fixtures/templateLaTeX.latex").open("rb"),
        "csl_file": Path("tests/fixtures/chicagomodified.csl").open("rb"),
    }
    response = client.post(url, params={"with_link_citations": "true"}, files=files)
    assert response.status_code == 200
    assert response.headers["content-type"] == "text/tex; charset=utf-8"
    assert (
        "C'est article est un exemple d'article type édité sur \\emph{Stylo}."
        in response.content.decode()
    )
    assert "\\citeproc{ref-goody_raison_1979}{Goody 1979}" in response.content.decode()
    assert (
        "\\bibitem[\\citeproctext]{ref-goody_raison_1979}" in response.content.decode()
    )
