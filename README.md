# Une API pour Pandoc

## Installation

Pré-requis : Python3, Pandoc et pdflatex/lualatex
(`brew install --cask mactex` ou `apt-get install texlive-full`).

Pour les conversions XSLT, il est aussi nécessaire d’avoir `saxon`
dans son `$PATH` (`brew install saxon` sous macOS).

Installer et activer un environnement virtuel :

    $ python3 -m venv venv
    $ source venv/bin/activate

Installer les dépendances :

    $ make install


## Environnement

Créer un fichier d’environnement :

    $ touch .env

Y mettre la commande XSL propre à l’environnement, ça peut être (macOS) :

    XSL_COMMAND="saxon"

ou si vous êtes sous Linux (à adapter) :

    XSL_COMMAND="java -cp ./saxon/saxon-he-11.4.jar net.sf.saxon.Transform"

D’autres variables sont définies dans le fichier `app/config.py`, notamment la version.
Vous pouvez les écraser dans votre fichier `.env`.


## Lancement

Lancer le serveur local :

    $ make run

Aller sur http://127.0.0.1:8000/


## Développement

Installer les dépendances :

    $ make dev


### Lancer les tests

Lancer la commande :

    $ make test

Optionnellement, la commande `ptw` permet de rafraîchir le lancement des
tests à chaque modification du code (ou des tests).


### Contribuer

Il faut s’assurer préallablement :

1. Que les tests passent : `make test`
2. Que le code coverage soit au-dessus de 90% dans le résultat ci-dessus
3. Que le code soit propre : `make lint`


### Ajouter une dépendance au projet

1. Ajouter le nom de la dépendance dans `pyproject.toml`.
2. Lancer `make deps`
3. Installer les nouvelles dépendances : `make dev`
